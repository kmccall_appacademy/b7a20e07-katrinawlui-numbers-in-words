class Fixnum
  def in_words
    if self < 20
      convert_0_to_20(self)
    elsif self < 100
      convert_20_to_99(self)
    elsif self < 1_000_000_000_000_000
      convert_100_to_999999999999999(self)
    end
  end

  private

  def convert_0_to_20(num)
    strings = [
      "zero", "one", "two", "three", "four", "five", "six", "seven",
      "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen",
      "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"
    ]
    strings[num]
  end

  def convert_20_to_99(num)
    strings = [
      "twenty", "thirty", "forty", "fifty", "sixty", "seventy",
      "eighty", "ninety"
    ]
    words = [strings[(num - 20) / 10]]
    words << (num % 10).in_words unless num % 10 == 0
    words.join(" ")
  end

  def convert_100_to_999999999999999(num)
    strings = {
      100 => "hundred", 1000 => "thousand", 1000000 => "million",
      1000000000 => "billion", 1000000000000 => "trillion"
    }
    divisor = hundred_to_trillion_divisor(num)

    words = ["#{(self / divisor).in_words} #{strings[divisor]}"]
    words << (num % divisor).in_words unless num % divisor == 0
    words.join(" ")
  end

  def hundred_to_trillion_divisor(num)
    if num < 1000
      100
    elsif num < 1000000
      1000
    elsif num < 1000000000
      1000000
    elsif num < 1000000000000
      1000000000
    elsif num < 1000000000000000
      1000000000000
    end
  end
end
